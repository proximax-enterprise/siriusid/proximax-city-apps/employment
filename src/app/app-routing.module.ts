import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'application', pathMatch: 'full' },
  // { path: 'application', loadChildren: () => import('./application/application.module').then( m => m.ApplicationPageModule)},
  {  path: 'application', loadChildren: './application/application.module#ApplicationPageModule'},
  { path: 'check-result', loadChildren: './check-result/check-result.module#CheckResultPageModule' },
  { path: 'show-result', loadChildren: './show-result/show-result.module#ShowResultPageModule' },
  { path: '**', redirectTo: 'application', pathMatch: 'full' },
  // { path: 'submit-infomation', loadChildren: './submit-infomation/submit-infomation.module#SubmitInfomationPageModule' },
  // { path: 'payment', loadChildren: './payment/payment.module#PaymentPageModule' },
  // { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule' },
  // { path: 'application', loadChildren: './application/application.module#ApplicationPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
